class RemoveUserMigrationFromUserIdeaLikes < ActiveRecord::Migration
  def change
  	remove_index :user_idea_likes, :user_id
  end
end
