class AddBlurbAndBasedInAndWanderlustToUser < ActiveRecord::Migration
  def change
    add_column :users, :blurb, :string
    add_column :users, :based_in, :integer
    add_column :users, :wanderlust, :integer
  end
end
