class CreateAlbumToPhotoRelationships < ActiveRecord::Migration
  def change
    create_table :album_to_photo_relationships do |t|
      t.integer :photo_id
      t.integer :album_id

      t.timestamps
    end
  end
end
