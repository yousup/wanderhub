class AddDefaultIdToPhotoalbum < ActiveRecord::Migration
  def change
    add_column :photoalbums, :default_id, :integer
  end
end
