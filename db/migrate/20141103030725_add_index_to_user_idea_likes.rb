class AddIndexToUserIdeaLikes < ActiveRecord::Migration
  def change
  	add_index :user_idea_likes, :user_id
  end
end
