class CreatePlaceToIdeaRelationships < ActiveRecord::Migration
  def change
    create_table :place_to_idea_relationships do |t|
      t.integer :place_id
      t.integer :idea_id

      t.timestamps
    end
  end
end
