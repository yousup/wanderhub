class CreateCityToAttractionRelationships < ActiveRecord::Migration
  def change
    create_table :city_to_attraction_relationships do |t|
      t.integer :city_id
      t.integer :attraction_id

      t.timestamps
    end
  end
end
