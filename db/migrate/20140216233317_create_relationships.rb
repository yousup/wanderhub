class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :trip_id
      t.integer :place_id

      t.timestamps
    end
    add_index :relationships, :trip_id
    add_index :relationships, :place_id
    add_index :relationships, [:trip_id, :place_id], unique: true
  end
end
