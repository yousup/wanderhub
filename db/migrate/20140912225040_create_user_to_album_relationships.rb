class CreateUserToAlbumRelationships < ActiveRecord::Migration
  def change
    create_table :user_to_album_relationships do |t|
      t.integer :user_id
      t.integer :album_id

      t.timestamps
    end
  end
end
