class CreateUserToIdeaRelationships < ActiveRecord::Migration
  def change
    create_table :user_to_idea_relationships do |t|
      t.integer :user_id
      t.integer :idea_id

      t.timestamps
    end
  end
end
