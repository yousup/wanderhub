class CreateIdeaToPhotoRelationships < ActiveRecord::Migration
  def change
    create_table :idea_to_photo_relationships do |t|
      t.integer :idea_id
      t.integer :photo_id

      t.timestamps
    end
  end
end
