class CreateCategoryRelationships < ActiveRecord::Migration
  def change
    create_table :category_relationships do |t|
      t.integer :place_id
      t.integer :cat_id

      t.timestamps
    end

    add_index :category_relationships, :place_id
    add_index :category_relationships, :cat_id
  end
end
