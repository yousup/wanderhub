# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141103030725) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "album_to_photo_relationships", force: true do |t|
    t.integer  "photo_id"
    t.integer  "album_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categories", force: true do |t|
    t.string   "cat"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "category_relationships", force: true do |t|
    t.integer  "place_id"
    t.integer  "cat_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "category_relationships", ["cat_id"], name: "index_category_relationships_on_cat_id", using: :btree
  add_index "category_relationships", ["place_id"], name: "index_category_relationships_on_place_id", using: :btree

  create_table "city_to_attraction_relationships", force: true do |t|
    t.integer  "city_id"
    t.integer  "attraction_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "idea_to_photo_relationships", force: true do |t|
    t.integer  "idea_id"
    t.integer  "photo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ideas", force: true do |t|
    t.string   "idea"
    t.integer  "likes"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "photoalbums", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "default_id"
  end

  create_table "photos", force: true do |t|
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
  end

  create_table "place_to_idea_relationships", force: true do |t|
    t.integer  "place_id"
    t.integer  "idea_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "places", force: true do |t|
    t.string   "name"
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "area"
    t.string   "description"
    t.string   "image_url"
    t.string   "coords"
  end

  add_index "places", ["name"], name: "index_places_on_name", unique: true, using: :btree

  create_table "relationships", force: true do |t|
    t.integer  "trip_id"
    t.integer  "place_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relationships", ["place_id"], name: "index_relationships_on_place_id", using: :btree
  add_index "relationships", ["trip_id", "place_id"], name: "index_relationships_on_trip_id_and_place_id", unique: true, using: :btree
  add_index "relationships", ["trip_id"], name: "index_relationships_on_trip_id", using: :btree

  create_table "trips", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
  end

  create_table "user_idea_likes", force: true do |t|
    t.integer  "user_id"
    t.integer  "idea_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "user_idea_likes", ["user_id"], name: "index_user_idea_likes_on_user_id", using: :btree

  create_table "user_to_album_relationships", force: true do |t|
    t.integer  "user_id"
    t.integer  "album_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_to_idea_relationships", force: true do |t|
    t.integer  "user_id"
    t.integer  "idea_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "remember_token"
    t.string   "first"
    t.string   "last"
    t.string   "blurb"
    t.integer  "based_in"
    t.integer  "wanderlust"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree

end
