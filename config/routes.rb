Peregrination::Application.routes.draw do

    # If database is locked run this in the rails console: 
    # ActiveRecord::Base.connection.execute("BEGIN TRANSACTION; END;")

    resources :trips, only:[:index, :new, :show, :create, :update, :destroy] 
    resources :place 
    resources :relationships, only:[:create, :destroy]
    resources :users
    resources :sessions, only:[:new, :create, :destroy]
    resources :photos
    resources :photoalbums
    resources :ideas

    root 'static_pages#home'
    match '/start',    to: 'static_pages#start',    via: 'get'
    match '/planner',  to: 'static_pages#planner',  via: 'get'
    match '/home', 	   to: 'static_pages#home',	   via: 'get'
    match '/about',    to: 'static_pages#about',    via: 'get'
    match '/form',      to: 'static_pages#form',    via: 'get'
    match "users/:id/prof_pic_upload", to: 'users#prof_pic_upload', :as => :prof_pic_upload, via: 'get'
    match "users/:id/edit_profile", to: "users#edit_profile", :as => :edit_profile, via: "get"
    match "photos/:id/edit_form",  to: "photos#edit_form", :as=>:edit_form, via: "get"
    match "ideas/:id/edit_likes", to: "ideas#edit_likes", :as=>:edit_likes, via: "post"
    # The priority is based upon order of creation: first created -> highest priority.
    # See how all your routes lay out with "rake routes".

    # You can have the root of your site routed with "root"
    # root 'welcome#index'

    # Example of regular route:
    #   get 'products/:id' => 'catalog#view'

    # Example of named route that can be invoked with purchase_url(id: product.id)
    #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

    # Example resource route (maps HTTP verbs to controller actions automatically):
    #   resources :products

    # Example resource route with options:
    #   resources :products do
    #     member do
    #       get 'short'
    #       post 'toggle'
    #     end
    #
    #     collection do
    #       get 'sold'
    #     end
    #   end

    # Example resource route with sub-resources:
    #   resources :products do
    #     resources :comments, :sales
    #     resource :seller
    #   end

    # Example resource route with more complex sub-resources:
    #   resources :products do
    #     resources :comments
    #     resources :sales do
    #       get 'recent', on: :collection
    #     end
    #   end

    # Example resource route with concerns:
    #   concern :toggleable do
    #     post 'toggle'
    #   end
    #   resources :posts, concerns: :toggleable
    #   resources :photos, concerns: :toggleable

    # Example resource route within a namespace:
    #   namespace :admin do
    #     # Directs /admin/products/* to Admin::ProductsController
    #     # (app/controllers/admin/products_controller.rb)
    #     resources :products
    #   end
end
