October 12th: 
	Ideas:
		1. Implement "Edit" link (/)
		2. Display "likes" (/)
		3. Display User of the Idea (/)
		4. Style Ideas on the Place page (/)
		5. Make ideas navigable (next, previous) *
		6. Make an option where you can display ideas in certain orders (newest, most popular, etc) *

	Photos/Photoalbums:
		1. Fix Photo additions via Idea (/)
		2. Style Photoalbum show (/)
		3. Photo descriptions (/)
		4. Implement Photo deletion and description edit (/)
		5. Implement Intra-album photo browsing (/)

	Places:
		1. Display what city a type "2" place is in (/)
		2. Add option to directly add an Idea in a Place page (/)

	Wishlists:
		1. Implement reorder capabilities *

	User:
		1. Implement feed for current wanderlust (both User show and Place [type 0] show) (/)
		2. Make it so that more feed as you scroll (both User show and Place [type 0] show) *

	About: 
		1. Make the About page
		2. Redo the home page *





Vision: Create games out of real life. 
- i.e photoshoots, hide and seek, keeping your mind busy, make it as if i'm incorporating "pinterest for adventuring". Encourage memories

combine foursquare, facebook, pinterest, spotify, 

To do list:

- relationships with categories <check>
- fill out a complete page for type 0,1, and 2 Places
- think of a better idea on the overall product instead of an auto-itinerary planner
	- divide into hangout or adventure or ??? <check>

- change home page to start off with 2 options <check>

- weather api (this should be fun)

- a back button the questions <check>

- waiting scrolly wheel as planner/manual search pages load (low priority)

- Come up with a "Touristy" scale and a "Hangout" scale to determine automatic planner <<<<<<<<<<============ *important
	- rank?
	- an assigned value?
	- how to assign value/rank? 
		- weighted by attributes (close-by: 50%, touristy: 25%, landmark 15%, etc)
		- account for proximity to the previous destination

- Change Place model to standardize what can and can't go in the database

- JSONify everything...?


Current list of categories:
1. Tourist
2. Hangout
3. Recreation
4. Museum
5. Park
6. Landmark
7. Food
8. Drink
9. Bar
10. Nightlife
11. Shopping

10% fun, 20% skill, 15% dark roast coffee with no room for fill, 5% javascript, 50% rails, and 100% reason to hire my ass