require 'spec_helper'

describe "Static Pages" do

	subject {page}

  	describe "visit home page" do
    before {visit root_path}
    
    it {should have_title('Journey Q') }
    it {should_not have_title('| Home')}
    end
end
