class IdeasController < ApplicationController
	def new
		@user = User.find(params[:user])
        if params[:place]
            @place = Place.find(params[:place])
        end
		respond_to do |format|
			format.html 
			format.js 
		end
	end

    def index
    	@user = User.find(params[:user])
    	@ideas = []
    	@user.user_to_idea_relationships.order('created_at DESC').each do |r|
    		idea = Idea.find(r.idea_id)
    		@ideas.append([Place.find(idea.place_to_idea_relationships.first.place_id), idea])
    	end
    end

    def show
        @place = Place.find(params[:place_id])
        @idea = Idea.find(params[:id])
        @idea_image = @idea.photos.first
        @user = @idea.user.first
        if UserIdeaLikes.where(user_id: current_user.id).where(idea_id: @idea.id).first
            @type = "-Fave"
        else
            @type = "+Fave"
        end
        respond_to do |format|
            format.html
            format.js
        end
    end 

    def destroy
        @user = User.find(params[:user])
        idea = Idea.find(params[:id])
        idea.idea_to_photo_relationships.each do |r|
            Photos.find(r.photo_id).destroy
        end
        idea.destroy
        redirect_to :back
    end

    def create
        # When creating an Idea with a photo, make sure to create both 
        # a AlbumToPhotoRelationship (with album.name = "Idea Photos")
        # and an IdeaToPhotoRelationship
        @idea = Idea.new
        
        @user = User.find(params[:idea][:user_id])
        @album = @user.albums.find_by_name("Idea Photos")

        # Gotta handle Place search
        @place = Place.search(params[:idea][:place].downcase).first
        if !@place
            flash[:error] = "Oops, unidentifiable place!"
            redirect_to ideas_path(:user => @user.id) and return
        end
        
        if params[:idea][:idea].empty?
            flash[:error] = "The idea is empty"
            redirect_to ideas_path(:user => @user.id) and return
        elsif !@place
            flash[:error] = "The idea has to be about somewhere!"
            redirect_to ideas_path(:user => @user.id) and return
        else
            @idea.update_attribute("idea", params[:idea][:idea])
            PlaceToIdeaRelationship.create!(place_id: @place.id, idea_id: @idea.id)
        end
        if params[:idea][:image]
            img = Photos.create!(image: params[:idea][:image])
            AlbumToPhotoRelationship.create!(album_id: @album.id, photo_id: img.id)
            IdeaToPhotoRelationship.create!(idea_id: @idea.id, photo_id: img.id)
        end

            UserToIdeaRelationship.create!(user_id: @user.id, idea_id: @idea.id)
        if @idea.save
            if params[:idea][:redirect_to_place]
                redirect_to place_path(:id=>@place.id, :user => @user.id) and return
            end
            redirect_to ideas_path(:user => @user.id) and return
        else
            flash[:error] = "Ooops something went wrong"
            redirect_to ideas_path(:user => @user.id) and return
        end
        
    end

    def update
        @idea = Idea.find(params[:id])
        
        @user = User.find(params[:idea][:user_id])
        @album = @user.albums.find_by_name("Idea Photos")

        # Gotta handle Place search
        @place = Place.find(params[:idea][:place])
        
        if params[:idea][:idea].empty?
            flash[:error] = "The idea is empty"
            redirect_to ideas_path(:user => @user.id) and return
        else
            @idea.update_attribute("idea", params[:idea][:idea])
        end
        if params[:idea][:image]
            img = Photos.create!(image: params[:idea][:image])
            AlbumToPhotoRelationship.create!(album_id: @album.id, photo_id: img.id)
            if @idea.photos.first
                IdeaToPhotoRelationship.destroy(IdeaToPhotoRelationship.find_by_idea_id(@idea.id).id)
            end
            IdeaToPhotoRelationship.create!(idea_id: @idea.id, photo_id: img.id)
        end

        if @idea.save
            redirect_to :back and return
        else
            flash[:error] = "Ooops something went wrong"
            redirect_to :back and return
        end
    end

    def edit_likes
        @idea = Idea.find(params[:id])
        @user = User.find(params[:user])

        if params[:type] == "+Fave"
            UserIdeaLikes.create!(user_id: @user.id, idea_id: @idea.id)
            @idea.increment!(:likes)
            @type = "-Fave"
        elsif params[:type] == "-Fave"
            UserIdeaLikes.where(user_id: @user.id).where(idea_id: @idea.id).first.destroy
            @idea.decrement!(:likes)
            @type = "+Fave"
        end

        respond_to do |format|
            format.html 
            format.js 
        end
    end

    def idea_params
        params.require(:idea).permit(:place, :idea, :user_id, :image)
    end
end
