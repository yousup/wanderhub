class PlaceController < ApplicationController
	require 'open-uri'
	require 'json'

	def show
		# If there was a search performed
		if params[:term]
			@autocompletes = nav_search_bar()[0..5]
		else
			@autocompletes = Place.all
		end

		# No navbar search ajax, yes navbar search submit
		if params[:form] and !params[:term]
			# If short search, just go back
			if params[:form][:query].length < 3
				redirect_to :back and return
			end
			@place = Place.search(params[:form][:query].downcase).first || not_found

			if @place.type == "0"
				stuff = place_is_city()
				@data = stuff[0]
				@region = stuff[1]
				@current_temp = stuff[2]
				@forecast = stuff[3]
				@attractions = stuff[4]
				@feed_items = Idea.city_ideas_aggregate(@place.id)
			elsif @place.type == "2"
				@ideas = place_is_2
			end
			render 'show' and return
		# Neither navbar search submit or autocomplete request (i.e url or clicking a link)
		elsif !params[:form] and !params[:term]
			if params[:id].to_i != 0
				@place = Place.find(params[:id].to_i)
			else
				@place = Place.find_by_name(params[:id].downcase)
			end
			
			if @place.type == "0"
				stuff = place_is_city()
				@data = stuff[0]
				@region = stuff[1]
				@current_temp = stuff[2]
				@forecast = stuff[3]
				@attractions = stuff[4]
				@feed_items = Idea.city_ideas_aggregate(@place.id)
			elsif @place.type == "2"
				@ideas = place_is_2
			end
		end

		respond_to do |format|
			format.html { render 'show'}
			format.js
			format.json { render :json => @autocompletes.to_json}
		end
	end

	####################### THIS INDEX METHOD IS VERY REFACTORABLE #######################
	####################### THIS INDEX METHOD IS VERY REFACTORABLE #######################
	####################### THIS INDEX METHOD IS VERY REFACTORABLE #######################
	def index
		# If there's a specific request (i.e not a search) then handle them first (display, add, remove)
		if params[:page] == nil
			@page = '1'
		else
			@page = params[:page]
		end

		# For now. This is Austin
		@current = [30.267153, -97.7430608]
		# Autocomplete case for drop down fill out on search bar
		if params[:term]
			@autocompletes = nav_search_bar()
		else
			@autocompletes = Place.all
		end

		if params[:request] == "display"
			@tripid = params[:tripid]
			@temptrip = params[:store]
			@name = Trip.find(@tripid).name
			@page = nil
			@search = nil
			render "place/_index" and return
		elsif params[:request] == "add"
			if params[:tripid] != nil
				@tripid = params[:tripid]
			end

			@place = Place.find(params[:id])
			@temptrip = params[:store].uniq
			@current = params[:current]
			if params[:search]
				@places = Place.search(params[:search]).uniq#.paginate(:page => params[:page], :per_page => 5)
			else
				# For some reason this is an array of strings. need it to be array of objects
				@places = []
				params[:places].each do |p|
					@places.push(Place.find_by_name(p))
				end
				@places = @places.paginate(page: 1, per_page: 5).uniq
			end
			@search = params[:search]
		elsif params[:request] == "remove"
			if params[:tripid] != nil
				@tripid = params[:tripid]
			end
			@place = Place.find(params[:id])
			if params[:search]
				@places = Place.search(params[:search]).uniq#.paginate(:page => params[:page], :per_page => 5)
			else
				# For some reason this is an array of strings. need it to be array of objects
				@places = []
				if params[:places] and !params[:places].empty?
					params[:places].each do |p|
						@places.push(Place.find_by_name(p))
					end
					# @places = @places.paginate(page: 1, per_page: 5)
				end
			end
			@search = params[:search]
			@temptrip = params[:store] 
			@current = params[:current]
		# Else it is a search query. Display search results accordingly
		else
			# This is the case where we click on paginated page of results
			if params[:query] != nil
				@search = params[:query]
				@places = Place.search(@search)#.paginate(:page => params[:page], :per_page => 5)
				if params[:store] != nil
				@temptrip = params[:store]
				end
				if params[:tripid] != nil
					@tripid = params[:tripid]
				end
			# This is after an actual search or re-search
			elsif params[:f] != nil
				if params[:f][:query] == ""
					if params[:f][:searchtype]
						redirect_to :back and return
					else
						redirect_to start_path and return
					end
				end
				if params[:f][:store] != nil
					@temptrip = params[:f][:store]
				else
					@temptrip = []
				end
				if params[:f][:tripid] != nil
					@tripid = params[:f][:tripid]
				end
				@search = params[:f][:query]
				if params[:f][:query].to_s != " "
					@places = Place.search(@search).uniq#.paginate(:page => params[:page], :per_page => 5)
				else
					Place.scoped
				end
			end
		end
		respond_to do |format|
			format.html { render '_index'}
			format.js
			format.json { render :json => @autocompletes.to_json}
		end
	end

	private

	def nav_search_bar
		attractions = Place.joins('INNER JOIN city_to_attraction_relationships ON city_to_attraction_relationships.city_id = places.id').select('places.name as name, city_to_attraction_relationships.attraction_id as attraction_id').where('places.name LIKE ?', "%#{params[:term]}%")
		# attractions2 = attractions.joins('INNER JOIN places as places2 ON attractions.attraction_id = places2.id')
		places = []
		attractions.to_a.each do |item|
			places.push(Place.find(item.attraction_id))
		end

		return 	(Place.find(:all, :conditions => ['name LIKE ?', "%#{params[:term]}%"]) +
				places +	
				Place.find(:all, :conditions => ['description LIKE ?', "%#{params[:term]}%"])).uniq
	end

	def place_is_2
		if @place.place_to_idea_relationships
			ideas = []
			@place.place_to_idea_relationships.each do |r|
		  		idea = Idea.find(r.idea_id)
		  		ideas.append(idea)
		  	end
	  	end
	  	return ideas
	end

	def place_is_city
		data = Geocoder.search(@place.name)[0]

		if data.country_code == "US"
			region = data.state_code
		else
			region = data.country_code
			if region == "GB"
				region = "UK"
			end
		end

		# attraction_coordinates = '{'
		# count = 0
		# @place.attractions.each do |item|
		# 	attraction_coordinates+=('"'+count.to_s+':"'+item.coords["c1"].to_s+',')
		# 	count+=1
		# end
		# attraction_coordinates = attraction_coordinates[0..-2]+'}'

		open("http://api.wunderground.com/api/19dc3aa31861691d/forecast/conditions/q/#{region}/#{data.city.gsub(/[ ]/,'_')}.json") do |f|
			json_string = f.read
			weather = JSON.parse(json_string)
			if weather
				current_temp = weather['current_observation']['temp_f']
				forecast = weather['forecast']['simpleforecast']['forecastday']
				return [data, region, current_temp, forecast, @place.attractions[0..6]]
			end
		end
	end
end
