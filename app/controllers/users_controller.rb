class UsersController < ApplicationController
	def new
		@static = '1'
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			profilepics = Photoalbum.create!(name: "Profile Pictures", default_id: 1)
			ideapics = Photoalbum.create!(name: "Idea Photos", default_id: 1)
			# AlbumToPhotoRelationship.create!(album_id: profilepics.id, photo_id: 1)
			# AlbumToPhotoRelationship.create!(album_id: ideapics.id, photo_id: 1)
			UserToAlbumRelationship.create!(album_id: profilepics.id, user_id: @user.id)
			UserToAlbumRelationship.create!(album_id: ideapics.id, user_id: @user.id)
			
			# Default wanderlust and based_in is Chicago
			@user.update_attribute("wanderlust", 3)
			@user.update_attribute("based_in", 3)

			sign_in @user
			flash[:new] = "Thanks for making an account with us!"
			redirect_to @user
		else
			flash[:error] = "Oops, something went wrong. Please try again"
			@static = '1'
			render 'new'
		end
	end 

	def prof_pic_upload
		@user = User.find(params[:id])
		@album = @user.albums.find_by_name(params[:album])
	    respond_to do |format|
	      format.html {render 'shared/_prof_pic_upload'}
	      format.js 
	    end
  	end

  	def edit_profile
		@user = User.find(params[:id])
	    respond_to do |format|
	      format.html
	      format.js 
	    end
  	end

	def show
		@user = User.find(params[:id])
		@wanderlust = Place.find(@user.wanderlust)
		# Leave as Austin for now
		@current = [41.8819, -87.6278] # This is Austin
		# @current = [request.location.latitude, request.location.longitude]
		# if !@user
		# 	@static = "1"
		# 	render 'sessions/new' and return
		# end
		@feed_items = Idea.city_ideas_aggregate(@wanderlust.id)
	end

	def update
		user = User.find(params[:user][:user_id])
		flash[:update_error] = ""
		if current_user = user

			# If user updated blurb
			if params[:user][:blurb] and !params[:user][:blurb].empty?
				if params[:user][:blurb].length > 140
					flash[:update_error] += "Your blurb can only be a max of 140 characters!"
					redirect_to :back and return
				end
			end

			if params[:user][:based_in] and !params[:user][:based_in].empty?
				new_based_candidates = Place.search(params[:user][:based_in].downcase)
				new_based = 0
				new_based_candidates.each do |c|
					if c.type == "0"
						new_based = c.id
						break
					end
				end
				if !new_based or new_based == 0
					flash[:update_error] += "Oops, your new base does not seem to be a valid city"
					redirect_to :back and return
				end
			end

			if params[:user][:wanderlust] and !params[:user][:wanderlust].empty?
				new_wanderlust_candidates = Place.search(params[:user][:wanderlust].downcase)
				new_wanderlust = 0
				new_wanderlust_candidates.each do |c|
					if c.type == "0"
						new_wanderlust = c.id
						break
					end
				end
				if !new_wanderlust or new_wanderlust == 0
					flash[:update_error] += "Oops, your new wanderlust is not a valid city"
					redirect_to :back and return
				end
			end

			if params[:user][:blurb] and !params[:user][:blurb].empty?
				user.update_attribute("blurb", params[:user][:blurb])
			end
			if params[:user][:based_in] and !params[:user][:based_in].empty?
				user.update_attribute("based_in", new_based)
			end
			if params[:user][:wanderlust] and !params[:user][:wanderlust].empty?
				user.update_attribute("wanderlust", new_wanderlust)
			end
			redirect_to user
		end
	end

	private

	def user_params
		params.require(:user).permit(:first, :last, :email, :password, :password_confirmation, :blurb, :based_in, :wanderlust)
	end
end
