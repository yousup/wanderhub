class SessionsController < ApplicationController
	def new
		@static = '1'
	end 

	def create
		@current = [41.9290629, -87.6459988] # For now
		user = User.find_by(email: params[:session][:email].downcase)
		if user && user.authenticate(params[:session][:password])
			sign_in user
			@user = user
			@wanderlust = Place.find(@user.wanderlust)
			# Leave as Austin for now
			@current = [41.8819, -87.6278] # This is Austin
			# @current = [request.location.latitude, request.location.longitude]
			# if !@user
			# 	@static = "1"
			# 	render 'sessions/new' and return
			# end
			@feed_items = Idea.city_ideas_aggregate(@wanderlust.id)
			render 'users/show'
		else
			flash[:error] = "Invalid User/Password Combination"
			@static = '1'
			render 'new'
		end
	end 

	def destroy
		sign_out
		redirect_to root_path
	end
end
