class StaticPagesController < ApplicationController
	include Geocoder

	def home
		@static = true
		respond_to do |format|
			format.html { render '_home'}
			format.js
		end
	end

	def planner
		@static = true
		# This is necessary because of development environment
		# if request.remote_ip.to_s != "127.0.0.1"
		# 	# temp_city = Geocoder.search( request.remote_ip )[0]
		# 	@city = Geocoder.search("#{request.location.latitude.to_s}, + #{request.location.longitude.to_s}")[0].city
		# else
		# 	@city = Geocoder.search("Seoul")[0].city
		# end
		respond_to do |format|
			format.html { render '_planner'}
			format.js
		end
	end

	def about
	end

	def form
		respond_to do |format|
			format.html { render '_form'}
			format.js
		end
	end

	def start
		@static = true
		respond_to do |format|
			format.html { render '_start'}
			format.js
		end
	end
end
