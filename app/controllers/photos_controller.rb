class PhotosController < ApplicationController
    def new
    end

    def edit_form
        @photo = Photos.find(params[:id])
        @album = Photoalbum.find(params[:album])
        @user = User.find(params[:user])
        respond_to do |format|
            format.html 
            format.js 
        end
    end

    def update
        @photo = Photos.find(params[:id])
        @photo.update_attribute("description", params[:photos][:description])
        @album = Photoalbum.find(params[:album])
        @user = User.find(params[:user])
        respond_to do |format|
            format.html 
            format.js 
        end
    end

    def show
        @user = User.find(params[:user])
        if params[:album]
            @album = Photoalbum.find(params[:album])
            if @album.name == "Profile Pictures"
                photos = @album.photos.order('created_at DESC')
            else
                photos = @album.photos.order('created_at ASC')
            end
        end

        @iteration = params[:id].to_i
        @photo = photos[params[:id].to_i]
        @prev = photos[params[:id].to_i - 1]
        @next = photos[params[:id].to_i + 1]
        respond_to do |format|
            format.html 
            format.js 
        end
    end

    def create
        if params[:photos][:case] == "profile-picture"
        	img = Photos.create!(image: params[:photos][:image])
        	just_uploaded_id = img.id
        	Photoalbum.find(params[:photos][:album]).set_default(just_uploaded_id)
        	AlbumToPhotoRelationship.create!(album_id: params[:photos][:album], photo_id: just_uploaded_id)
        	redirect_to User.find(params[:photos][:user_id]) and return
        end

    end

    def destroy
        photo = Photos.find(params[:id])
        photo.destroy
        redirect_to photoalbum_path(id: params[:album])
    end

    private

  	def photos_params
		params.require(:photos).permit(:image)
	end
end
