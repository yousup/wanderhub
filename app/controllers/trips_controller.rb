class TripsController < ApplicationController
	include TripsHelper
	include Math
	require 'will_paginate/array'

	# use 'new' to display a suggested trip by auto algo
	def new
		if params[:trips][:auto_location] == '0'
			# Need a way to figure out how to auto-complete and choose an actual location
			@current = Place.find_by_name(params[:trips][:location])
			if !@current
				flash[:invalid_place] = "Oops we don't know where you're searching"
				render 'static_pages/_planner'
				return
			else
				@current = Place.find_by_name(params[:trips][:location]).coords["c1"]
			end
		else
			# For now
			@current = [41.8781136, -87.6297982] # This is Chicago
			# @current = [request.location.latitude, request.location.longitude]
		end

		@time = params[:trips][:time_available]

		if params[:trips][:distance] != ''
			@distance = params[:trips][:distance].to_f
		else
			@static = '1'
			flash[:invalid_range] = "We have to know how far you can go!"
			render 'static_pages/_planner'
			return
		end

		if params[:trips][:purpose] == "hangout"
			flash[:message] = "Here are some friendly hangout places you can check out!"
		else
			flash[:message] = "Here are some new places you can check out!"
		end
		flash[:message2] = "Just add to your wishlist the places you want to visit"

		@places = auto_plan(params[:trips][:purpose], @current, @time, @distance, []).paginate(page: 1, per_page: 5)
		@generated = '1'
		render "place/_index" and return
	end

	def update
		if params[:trip][:store] == nil
			flash[:error] = "Your trip appears to be empty"
			redirect_to :back and return
		end
		if params[:trip][:store].empty?
			flash[:error] = "Your trip appears to be empty"
			redirect_to :back and return
		end
		@trip = Trip.find(params[:id])
		# really inefficient i think
		@trip.relationships.destroy_all
		params[:trip][:store].each do |name|
			Relationship.create!(trip_id: @trip.id, place_id: Place.find_by_name(name).id)
		end
		@trip.name = params[:trip][:name]
		@trip.save
		@user = User.find(@trip.user_id)
		redirect_to trip_path(:user => @trip.user_id)
	end

	# Create a new TRIP (i.e when user clicks "Save Trip")
	def create
		if params[:trips][:name] == ""
			flash[:nameless] = "Your wishlist needs a name!"
			@current = params[:trips][:current]
			@temptrip = params[:trips][:store]
			@places = []
			params[:trips][:places].each do |name|
				@places.push(Place.find_by_name(name))
			end
			render 'place/_index' and return
		end
		if params[:trips][:name] != nil and params[:trips][:name] != "" and current_user
			# Make new trip and save the name. Save the trip. 
			@trip = Trip.new
			@trip.name = params[:trips][:name]
			@trip.user_id = current_user.id
			@trip.save

			params[:trips][:store].each do |name|
				Relationship.create!(trip_id: @trip.id, place_id: Place.find_by_name(name).id)
			end
			@user = User.find(@trip.user_id)
			render 'show' and return
		end
	end

	# Show a currently existing trip
	def show
		@user = User.find(params[:user])
		@trip = Trip.find(params[:id])

	end

	def destroy
		if current_user == Trip.find(params[:id]).user
			Trip.destroy(params[:id])
			@user = current_user
			@wishlists = @user.trips.order('created_at DESC')
			render 'trips/index'
		end
	end

	def index
		@user = User.find(params[:user])
		@wishlists = @user.trips.order('created_at DESC')
	end	
end
