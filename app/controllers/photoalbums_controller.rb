class PhotoalbumsController < ApplicationController
	def new
		@user = User.find(params[:id])
		if params[:album]
			@album = Photoalbum.find(params[:album])
		end
		respond_to do |format|
			format.html 
			format.js 
		end
	end

	def index
		@user = User.find(params[:user])
		@albums = @user.albums.order('created_at ASC')
	end

	def update
		if current_user == User.find(params[:album][:user_id])
			@album = Photoalbum.find(params[:id])
			if params[:album][:name] and !params[:album][:name].empty?
				@album.update_attribute("name", params[:album][:name])
			end
			if params[:album][:images]
				params[:album][:images].each do |image|
					img = Photos.create!(image: image)
			  		just_uploaded_id = img.id
			  		AlbumToPhotoRelationship.create!(album_id: @album.id, photo_id: just_uploaded_id)
				end
			end
			redirect_to photoalbum_path(:id => params[:id], :user => params[:album][:user_id]) and return
		end
	end

	def create
		if current_user == User.find(params[:album][:user_id])
			new_album = Photoalbum.new
			if params[:album][:name].empty?
				name = "Untitled"
			else
				name = params[:album][:name]
			end
			new_album.name = name
			if new_album.save
				UserToAlbumRelationship.create!(user_id: current_user.id, album_id: new_album.id)
				if params[:album][:images]
					params[:album][:images].each do |image|
						img = Photos.create!(image: image)
				  		just_uploaded_id = img.id
				  		AlbumToPhotoRelationship.create!(album_id: new_album.id, photo_id: just_uploaded_id)
					end
				end
			end
			@album = new_album
			redirect_to @album and return
		end
	end

	def show
		@album = Photoalbum.find(params[:id])
		@user = @album.users.first
		if @album.name == "Profile Pictures"
			@photos = @album.photos.order('created_at DESC')
		else
			@photos = @album.photos.order('created_at ASC')
		end
	end

	def destroy
		if current_user == User.find(params[:user])
			Photoalbum.find(params[:id]).album_to_photo_relationships.all.each do |p|
				Photos.destroy(Photos.find(p.photo_id)) # For now
			end
			Photoalbum.destroy(params[:id])
			redirect_to photoalbums_path(:user => params[:user])
		end
	end
end
