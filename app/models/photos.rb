class Photos < ActiveRecord::Base
	validates :image, presence: :true
	mount_uploader :image, ImageUploader

	# Album to Photo relationship
	has_many :album_to_photo_relationships, foreign_key: "photo_id", dependent: :destroy
	has_many :albums, through: :album_to_photo_relationships, source: :album

	# Idea to Photo relationship
	has_many :idea_to_photo_relationships, foreign_key: "photo_id", dependent: :destroy
	has_many :ideas, through: :idea_to_photo_relationships, source: :idea
end
