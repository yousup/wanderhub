class Relationship < ActiveRecord::Base
	belongs_to :place, class_name: "Place"
	belongs_to :trip, class_name: "Trip"
	validates :trip_id, presence: true
  	validates :place_id, presence: true
end
