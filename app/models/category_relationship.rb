class CategoryRelationship < ActiveRecord::Base
	belongs_to :place, class_name: "Place"
	belongs_to :cat, class_name: "Category"
	validates :place_id, presence: true
	validates :cat_id, presence: true
end
