class Category < ActiveRecord::Base
	has_many :category_relationships, foreign_key: "cat_id", dependent: :destroy
	has_many :places, through: :category_relationships, source: :place
	before_save {self.cat = cat.upcase}
end
