class AlbumToPhotoRelationship < ActiveRecord::Base
	belongs_to :album, class_name: "Photoalbum"
	belongs_to :photo, class_name: "Photos"
	validates :photo_id, presence: :true
	validates :album_id, presence: :true 
end
