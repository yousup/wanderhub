class UserToAlbumRelationship < ActiveRecord::Base
	belongs_to :user, class_name: "User"
	belongs_to :album, class_name: "Photoalbum"
	validates :user_id, presence: :true
	validates :album_id, presence: :true
end
