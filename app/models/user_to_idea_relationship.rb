class UserToIdeaRelationship < ActiveRecord::Base
	belongs_to :user, class_name: "User"
	belongs_to :idea, class_name: "Idea"
	validates :user_id, presence: true
	validates :idea_id, presence: true
end
