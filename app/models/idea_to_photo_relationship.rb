class IdeaToPhotoRelationship < ActiveRecord::Base
	belongs_to :idea, class_name: "Idea"
	belongs_to :photo, class_name: "Photos"
	validates :idea_id, presence: :true
	validates :photo_id, presence: :true
end
