class CityToAttractionRelationship < ActiveRecord::Base
	belongs_to :city, class_name: "Place"
	belongs_to :attraction, class_name: "Place" 
	validates :city_id, presence: :true
	validates :attraction_id, presence: :true
end
