class User < ActiveRecord::Base
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: {with: VALID_EMAIL_REGEX}, uniqueness: {case_sensitive: false}
	before_save {self.email = email.downcase}
	before_save {self.first = first.titleize}
	before_save {self.last = last.titleize}
	has_many :trips, dependent: :destroy
	has_secure_password
	validates :password, length: {minimum: 6}
	validates :blurb, length: {maximum: 140}

	before_create :create_remember_token

	validates :first, presence: true
	validates :last, presence: true

	# User to Photoalbum relationship
  	has_many :user_to_album_relationships, foreign_key: "user_id", dependent: :destroy
  	has_many :albums, through: :user_to_album_relationships, source: :album

  	# User to Idea relationship
  	has_many :user_to_idea_relationships, foreign_key: "user_id", dependent: :destroy
  	has_many :ideas, through: :user_to_idea_relationships, source: :idea

  	# User to Idea likes
  	has_many :user_idea_likes, foreign_key: "user_id", dependent: :destroy

	def User.new_remember_token
		SecureRandom.urlsafe_base64
	end

	def User.hash(token)
		Digest::SHA1.hexdigest(token.to_s)
	end

	private
		def create_remember_token
			self.remember_token = User.hash(User.new_remember_token)
		end

		def person_params
		    params.require(:user).permit(:name, :email, :password, :password_confirmation)
	  	end
end
