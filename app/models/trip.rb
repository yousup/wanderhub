class Trip < ActiveRecord::Base
	has_many :relationships, foreign_key: "trip_id", dependent: :destroy
	has_many :places, through: :relationships, source: :place
	validates :name, presence: true
	validates :user_id, presence: true
	belongs_to :user
end
