class Photoalbum < ActiveRecord::Base
	validates :name, presence: :true

	# User to album relationship
	has_many :user_to_album_relationships, foreign_key: "album_id", dependent: :destroy
	has_many :users, through: :user_to_album_relationships, source: :user

	# Album to Photo relationship
	has_many :album_to_photo_relationships, foreign_key: "album_id", dependent: :destroy
	has_many :photos, through: :album_to_photo_relationships, source: :photo

	def set_default(id)
		self.update(default_id: id)
	end

	def default
		Photos.find(self.default_id)
	end
end
