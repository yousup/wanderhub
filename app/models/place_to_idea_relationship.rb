class PlaceToIdeaRelationship < ActiveRecord::Base
	belongs_to :place, class_name: "Place"
	belongs_to :idea, class_name: "Idea"
	validates :place_id, presence: true
	validates :idea_id, presence: true
end
