class UserIdeaLikes < ActiveRecord::Base
	belongs_to :idea, class_name: "Idea"
	belongs_to :user, class_name: "User"
	validates :user_id, presence: :true
	validates :idea_id, presence: :true
end
