class Place < ActiveRecord::Base
  	serialize :coords
  	self.inheritance_column = nil

	  validates :name, presence: :true, format: { with: /\A[()a-z '.,-]+[, ]*[a-z]*\z/,
    message: "only allows lowercase letters" }
    validates :area, format: { with: /\A[a-z ]*\z/,
    message: "only allows lowercase letters" }
    validates :type, format: { with: /\A[012]\z/,
    message: "can only be 0, 1, or 2" }
    validates :image_url, format: {with: /\A[a-z\/.]+\z/, message: "only lowercase letters, no spaces"}

    # Place to Wishlist relationship
    has_many :relationships, foreign_key: "place_id", dependent: :destroy
    has_many :trips, through: :relationships, source: :trip

    # Place to Category relationship
  	has_many :category_relationships, foreign_key: "place_id", dependent: :destroy
  	has_many :categories, through: :category_relationships, source: :cat

    # City to Attraction relationship (both are Places)
    has_many :city_to_attraction_relationships, foreign_key: "city_id", dependent: :destroy
    has_many :reverse_city_to_attraction_relationships, foreign_key: "attraction_id", class_name: "CityToAttractionRelationship", dependent: :destroy
    has_many :attractions, through: :city_to_attraction_relationships, :order=>'name'
    has_many :city, through: :reverse_city_to_attraction_relationships

    # Place to Idea relationship
    has_many :place_to_idea_relationships, foreign_key: "place_id", dependent: :destroy
    has_many :ideas, through: :place_to_idea_relationships, source: :idea

	def to_param
    	self.name
  	end

  	# This is an extremely simple search algorithm
  	def self.search(search)
	    # where(:title, query) -> This would return an exact match of the query
        if search
            attractions = Place.joins('INNER JOIN city_to_attraction_relationships ON city_to_attraction_relationships.city_id = places.id').
                select('places.name as name, city_to_attraction_relationships.attraction_id as attraction_id').where('places.name LIKE ?', "%"+search+"%")
            # attractions2 = attractions.joins('INNER JOIN places as places2 ON attractions.attraction_id = places2.id')
            places = []
            attractions.to_a.each do |item|
                places.push(Place.find(item.attraction_id))
            end
      	    search_condition = "%"+search+"%"
      	    
            x = where("name LIKE ?", search_condition) + places
            x.uniq
        end
  	end

    private

    def place_params
        params.require(:place).permit(:name, :type, :area, :coords, :image_url)
    end
end
