class Idea < ActiveRecord::Base
	validates :idea, presence: true
	before_create {self.likes = 0}

  	# User to Idea relationship
  	has_many :user_to_idea_relationships, foreign_key: "idea_id", dependent: :destroy
  	has_many :user, through: :user_to_idea_relationships, source: :user

  	# Place to Idea relationship
  	has_many :place_to_idea_relationships, foreign_key: "idea_id", dependent: :destroy
  	has_many :place, through: :place_to_idea_relationships, source: :place

  	# Idea to Photo relationship
  	has_many :idea_to_photo_relationships, foreign_key: "idea_id", dependent: :destroy
  	has_many :photos, through: :idea_to_photo_relationships, source: :photo

    # User Idea likes
    has_many :user_idea_likes, foreign_key: "idea_id", dependent: :destroy

    def self.city_ideas_aggregate(city_id)
      return Idea.joins("INNER JOIN place_to_idea_relationships ON place_to_idea_relationships.idea_id = ideas.id").
        joins('INNER JOIN city_to_attraction_relationships ON city_to_attraction_relationships.attraction_id = place_to_idea_relationships.place_id').
          where('city_to_attraction_relationships.city_id = ?', city_id).order("likes DESC")
    end
end
