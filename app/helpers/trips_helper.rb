module TripsHelper
	include Math

	# This is where the major algorithm goes!
	def auto_plan(purpose,location=[], time=0, distance=-1, modes=[])
		lat = location[0]
		long = location[1]

		# First get all the Places where coordinates are within radius of current location
		# The set of returned Places is treated as a Graph
		store = []

		# this currently does a linear pass of EVERY RECORD IN THE PLACE TABLE.
		# i am sure there are more efficient ways to do this
		
		Place.find_each do |place|
			# Find the closest place among sets of coordinates in coords:
			place.coords.each do |key, pair|
				templat = pair[0]
				templong = pair[1]

				# Only in the USA because we're the only country that uses miles and not km
				new_distance = distance * 1.60934
				rad = 0.0174532925
				# Compute distance to the place from entered location.
				radius = acos(sin(lat*rad) * sin(templat*rad) + cos(lat*rad) * cos(templat*rad) * cos(long*rad - (templong*rad))) * 6371

				if radius <= new_distance
					store.push(place)
					break
				end
			end
		end

		# <######################## Insert post-processing here ########################>

		# Post-process by purpose: if touristy or loungy
		store = process_purpose(store, purpose)

		# Post-process by time available
		store = process_time(store, time)
		
		# Post-process to display directions using Google Maps Directions API

		# Post-process to remove duplicates

		# Djikstra's algorithm? 
		return store
		
	end

	private 

	def process_purpose(store, purpose)
		newstore = []
		if purpose == "hangout"
			store.each do |place|
				if !place.categories.include? Category.find(2)
					newstore.push(place)
				end
			end
		elsif purpose == "tourist"
			store.each do |place|
				if !place.categories.include? Category.find(1)
					newstore.push(place)
				end
			end
		end
		return store - newstore
	end

	def process_time(store, time)
		return store
	end

end
