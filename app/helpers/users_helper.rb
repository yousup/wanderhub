module UsersHelper
	# Returns the Gravatar (http://gravatar.com/) for the given user.
	def gravatar_for(user)
		gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
		gravatar_url = "https://fbcdn-sphotos-f-a.akamaihd.net/hphotos-ak-xpa1/v/t1.0-9/944641_10151665025540846_1379513979_n.jpg?oh=cd856d70cc5c06092bd8ec7c099d4420&oe=547C95C0&__gda__=1416217371_09c0a46e31293e92d96c3ec496386c41"
		image_tag(gravatar_url, alt: user.email, class: "")
	end
end
